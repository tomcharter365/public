# How to install pip, pip2, & pip3 on babun
```
pact install python-devel python3
python3 -m ensurepip
python -m ensurepip
```
## pip should now be installed and you should be able to use `pip install` to install python packages.
* You may have to disconnect form the corporate network when installing packages

## Useful pip packages
1. `pssh` (https://www.tecmint.com/execute-commands-on-multiple-linux-servers-using-pssh/)
2. `beautysh` (https://github.com/bemeurer/beautysh)
