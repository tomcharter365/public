* Innocent face
	```
	ʘ‿ʘ
	```

* Reddit disapproval face
	```
	ಠ_ಠ
	```

* Table Flip / Flipping Table
	```
	(╯°□°）╯︵ ┻━┻
	```

* Put the table back
	```
	┬─┬﻿ ノ( ゜-゜ノ)
	```

* Tidy up / dust the table
	```
	┬─┬⃰͡ (ᵔᵕᵔ͜ )
	```

* Double Flip / Double Angry
	```
	┻━┻ ︵ヽ(`Д´)ﾉ︵﻿ ┻━┻
	```

* Fisticuffs
	```
	ლ(｀ー´ლ)
	```

* Cute bear
	```
	ʕ•ᴥ•ʔ
	```

* Cute face with big eyes
	```
	(｡◕‿◕｡)
	```

* surprised / loudmouthed
	```
	（　ﾟДﾟ）
	```

* shrug face
	```
	¯\_(ツ)_/¯
	```

* meh
	```
	¯\(°_o)/¯
	```

* feel perky
	```
	(`･ω･´)
	```

* angry face
	```
	(╬ ಠ益ಠ)
	```

* excited
	```
	☜(⌒▽⌒)☞
	```

* running
	```
	ε=ε=ε=┌(;*´Д`)ﾉ
	```

* happy face
	```
	ヽ(´▽`)/
	```

* basking in glory
	```
	ヽ(´ー｀)ノ
	```

* kitty emote
	```
	(ᵒᴥᵒ)ฅ
	```

* fido
	```
	V●ᴥ●V
	```

* meow
	```
	ฅ^•ﻌ•^ฅ
	```

* Cheers
	```
	（ ^_^）o自自o（^_^ ）
	```

* devious smile
	```
	ಠ‿ಠ
	```

* 4chan emoticon
	```
	( ͡° ͜ʖ ͡°)
	```

* crying face
	```
	ಥ_ಥ
	```

* breakdown
	```
	ಥ﹏ಥ
	```

* disagree
	```
	٩◔̯◔۶
	```

* flexing
	```
	ᕙ(⇀‸↼‶)ᕗ
	```

* do you even lift bro?
	```
	ᕦ(ò_óˇ)ᕤ
	```

* kirby
	```
	⊂(◉‿◉)つ
	```

* tripping out
	```
	q(❂‿❂)p
	```

* discombobulated
	```
	⊙﹏⊙
	```

* sad and confused
	```
	¯\_(⊙︿⊙)_/¯
	```

* japanese lion face
	```
	°‿‿°
	```

* confused
	```
	¿ⓧ_ⓧﮌ
	```

* confused scratch
	```
	(⊙.☉)7
	```

* worried
	```
	(´･_･`)
	```

* dear god why
	```
	щ（ﾟДﾟщ）
	```

* staring
	```
	٩(͡๏_๏)۶
	```

* pretty eyes
	```
	ఠ_ఠ
	```

* strut
	```
	ᕕ( ᐛ )ᕗ
	```

* zoned
	```
	(⊙_◎)
	```

* crazy
	```
	ミ●﹏☉ミ
	```

* trolling
	```
	༼∵༽ ༼⍨༽ ༼⍢༽ ༼⍤༽
	```

* angry troll
	```
	ヽ༼ ಠ益ಠ ༽ﾉ
	```

* fuck it
	```
	t(-_-t)
	```
* sad face
	```
	(ಥ⌣ಥ)
	```

* hugger
	```
	(づ￣ ³￣)づ
	```

* stranger danger
	```
	(づ｡◕‿‿◕｡)づ
	```

* flip friend
	```
	(ノಠ ∩ಠ)ノ彡( \o°o)\
	```

* cry face
	```
	｡ﾟ( ﾟஇ‸இﾟ)ﾟ｡
	```

* cry troll
	```
	༼ ༎ຶ ෴ ༎ຶ༽
	```

* TGIF
	```
	“ヽ(´▽｀)ノ”
	```

* dancing bear
	```
	┌(ㆆ㉨ㆆ)ʃ
	```

* sleepy
	```
	눈_눈
	```

* angry birds
	```
	( ఠൠఠ )ﾉ
	```

* no support
	```
	乁( ◔ ౪◔)「      ┑(￣Д ￣)┍
	```

* shy
	```
	(๑•́ ₃ •̀๑)
	```

* fly away
	```
	⁽⁽ଘ( ˊᵕˋ )ଓ⁾⁾
	```

* careless
	```
	◔_◔
	```

* love
	```
	♥‿♥
	```

* Touchy Feely
	```
	ԅ(≖‿≖ԅ)
	```

* Kissing
	```
	( ˘ ³˘)♥
	```

* shark face / jagged mustache
	```
	( ˇ෴ˇ )
	```

* dance
	```
	ヾ(-_- )ゞ
	```

* winnie the pooh
	```
	ʕ •́؈•̀ ₎
	```

* boxing / fight
	```
	ლ(•́•́ლ)
	```

* robot
	```
	{•̃_•̃}
	```

* seal
	```
	(ᵔᴥᵔ)
	```

* questionable / dislike
	```
	(Ծ‸ Ծ)
	```

* Winning!
	```
	(•̀ᴗ•́)و ̑̑
	```

* Zombie
	```
	[¬º-°]¬
	```

* pointing
	```
	(☞ﾟヮﾟ)☞
	```

* chasing / running away
	```
	''⌐(ಠ۾ಠ)¬'''
	```

* whistling / music
	```
	(っ•́｡•́)♪♬
	```

* injured
	```
	(҂◡_◡)
	```

* creeper
	```
	ƪ(ړײ)‎ƪ​​
	```

* acid
	```
	⊂(◉‿◉)つ
	```

(ㆆ _ ㆆ)
afraid

☜(⌒▽⌒)☞
angel

•`_´•
angry

⤜(ⱺ ʖ̯ⱺ)⤏
arrowhead

(‿|‿)
ass / butt

•͡˘㇁•͡˘
awkward

/|\ ^._.^ /|\
bat

ʕ·͡ᴥ·ʔ
bear / koala

ʕノ•ᴥ•ʔノ ︵ ┻━┻
bearflip

ʕっ•ᴥ•ʔっ
bearhug

()∵)
because / since

❤
bigheart

0__#
blackeye

( 0 _ 0 )
blubby

(˵ ͡° ͜ʖ ͡°˵)
blush

┌( ͝° ͜ʖ͡°)=ε/̵͇̿̿/’̿’̿ ̿
bond / 007

┌( ͝° ͜ʖ͡°)=ε[_]
cheers

( . Y . )
boobs

(-_-)
bored

( •͡˘ _•͡˘)ノð
bribe

( ˘ ³˘)ノ°ﾟº❍｡
bubbles

ƸӜƷ
butterfly

(= ФェФ=)
cat

( ͡° ᴥ ͡°)
catlenny

✓
check

╭(ʘ̆~◞౪◟~ʘ̆)╮
chubby

(͡ ° ͜ʖ ͡ °)
claro

ヽ༼ ຈل͜ຈ༼ ▀̿̿Ĺ̯̿̿▀̿ ̿༽Ɵ͆ل͜Ɵ͆ ༽ﾉ
clique / gang / squad

༼∵༽ ༼⍨༽ヽ༼ ຈᴥຈ༼ ▀̿̿Ĺ̯̿̿▀̿ ̿༽ʘ̆ᴥʘ̆ ༽ﾉ´◔ω◔༼༼ ຈᴥຈ༼ ° ͜ʖ͡° ̿༽Ɵ͆ᴥƟ͆ ༽ﾉ༼༼⍢༽ ༼⍤༽
Large group

☁
cloud

♣
club

c[_]
coffee / cuppa

⌘
cmd / command

(•_•) ( •_•)>⌐■-■ (⌐■_■)
cool / csi

©
copy / c

ԅ(≖‿≖ԅ)
creep

ƪ(ړײ)‎ƪ​​	creepcute

( ✜︵✜ )
crim3s

†
cross

(╥﹏╥)
cry

( ╥﹏╥) ノシ
crywave

(｡◕‿‿◕｡)
cute

⚀
d1

⚁
d2

⚂
d3

⚃
d4

⚄
d5

⚅
d6

(ᕗ ͠° ਊ ͠° )ᕗ
damnyou

ᕕ(⌐■_■)ᕗ ♪♬
dance

x⸑x
dead

(⌐■_■)
deal with it / dwi

(︶︹︶)
depressed

☉ ‿ ⚆
derp

♦
diamond

(◕ᴥ◕ʋ)
dog

[̲̅$̲̅(̲̅ιο̲̅̅)̲̅$̲̅]
dollarbill / $

(̿▀̿ ̿Ĺ̯̿̿▀̿ ̿)̄
dong

ヽ༼ຈل͜ຈ༽ﾉ
donger

╭∩╮（︶︿︶）╭∩╮
dontcare

ヽ(｀Д´)ﾉ
do not want / dontwant

<(^_^)>
dope

«
<<

»
>>

┻━┻ ︵ヽ(`Д´)ﾉ︵ ┻━┻
doubletableflip

↓	down
(・3・)	duckface
ᕕ(╭ರ╭ ͟ʖ╮•́)⊃¤=(————-	duel
(≧︿≦)	duh
¯\(°_o)/¯	dunno
ᴇʙᴏʟᴀ	ebola
…	ellipsis
	...
–	emdash
	--
☆	emptystar
△	emptytriangle
	t2
(҂◡_◡) ᕤ	endure
✉︎	envelope
	letter
ɛ	epsilon
€	euro
ψ(｀∇´)ψ	evil
(͠≖ ͜ʖ͠≖)	evillenny
(⌐■_■)︻╦╤─ (╥﹏╥)	execution
(╯°□°)╯︵ ʞooqǝɔɐɟ	facebook
(－‸ლ)	facepalm
вєωαяє, ι αм ƒαη¢у!	fancytext
(ˆ⺫ˆ๑)<3	fart
(ง •̀_•́)ง	fight
| (• ◡•)|	finn
<"(((<3	fish
卌	-5
	five
⅝	8-May
♭	flat
	bemolle
ᕙ(`▽´)ᕗ	flexing
ǝןqɐʇ ɐ ǝʞıן ǝɯ dıןɟ	fliptext
(ノ ゜Д゜)ノ ︵ ǝןqɐʇ ɐ ǝʞıן ʇxǝʇ dıןɟ	fliptexttable
┬─┬ ︵ /(.□. \）	flipped
	heavytable
(✿◠‿◠)	flower
	flor
✿	f
─=≡Σ((( つ◕ل͜◕)つ	fly
(╯°□°)╯︵ ┻━┻ ︵ ╯(°□° ╯)	friendflip
(ღ˘⌣˘ღ)	frown
୧༼ಠ益ಠ╭∩╮༽	fuckoff
	gtfo
┌П┐(ಠ_ಠ)	fuckyou
	fu
ಠ_ರೃ	gentleman
	sir
	monocle
= _ =	ghast
༼ つ ❍_❍ ༽つ	ghost
(´・ω・)っ由	gift
	present
༼ つ ◕_◕ ༽つ	gimme
(*・‿・)ノ⌒*:･ﾟ✧	glitter
(⌐ ͡■ ͜ʖ ͡■)	glasses
( ͡° ͜ʖ ͡°)ﾉ⌐■-■	glassesoff
(ﾉ☉ヮ⚆)ﾉ ⌒*:･ﾟ✧	glitterderp
(_゜_゜_)	gloomy
(з๏ε)	goatse
(☞ﾟ∀ﾟ)☞	gotit
( ´◔ ω◔`) ノシ	greet
	greetings
︻╦╤─	gun
	mg
༼つಠ益ಠ༽つ ─=≡ΣO))	hadouken
☭	hammerandsickle
	hs
☜	handleft
	hl
☞	handright
	hr
٩(^‿^)۶	haha
٩( ๑╹ ꇴ╹)۶	happy
ᕕ( ᐛ )ᕗ	happygarry
♥	h
	heart
(ʘ‿ʘ)╯	hello
	ohai
	bye
._.)/\(._.	highfive
( ｀皿´)｡ﾐ/	hitting
(づ｡◕‿‿◕｡)づ	hug
	hugs
┐｜･ิω･ิ#｜┌	iknowright
	ikr
୧(▲ᴗ▲)ノ	illuminati
∞	infinity
	inf
(っ´ω`c)♡	inlove
∫	int
ଘ(੭*ˊᵕˋ)੭* ̀ˋ ɪɴᴛᴇʀɴᴇᴛ	internet
‽	interrobang
(❍ᴥ❍ʋ)	jake
≧◡≦	kawaii
┬┴┬┴┤Ɵ͆ل͜Ɵ͆ ༽ﾉ	keen
~\(≧▽≦)/~	kiahh
(づ ￣ ³￣)づ	kiss
／人◕ ‿‿ ◕人＼	kyubey
λ	lambda
_(:3」∠)_	lazy
←	left
	<-
( ͡° ͜ʖ ͡°)	lenny
[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]	lennybill
(ง ͠° ͟ʖ ͡°)ง	lennyfight
(ノ ͡° ͜ʖ ͡°ノ) ︵ ( ͜。 ͡ʖ ͜。)	lennyflip
( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)	lennygang
¯\_( ͡° ͜ʖ ͡°)_/¯	lennyshrug
( ಠ ͜ʖ ರೃ)	lennysir
┬┴┬┴┤( ͡° ͜ʖ├┬┴┬┴	lennystalker
ᕦ( ͡° ͜ʖ ͡°)ᕤ	lennystrong
╰( ͡° ͜ʖ ͡° )つ──☆*:・ﾟ	lennywizard
███▒▒▒▒▒▒▒	loading
L(° O °L)	lol
(ಡ_ಡ)☞	look
♥‿♥	love
ʕ♥ᴥ♥ʔ	lovebear
꒰ ꒡⌓꒡꒱	lumpy
-`ღ´-	luv
ヽ(｀Д´)⊃━☆ﾟ. * ･ ｡ﾟ,	magic
(/¯◡ ‿ ◡)/¯ ~ ┻━┻	magicflip
\(°^°)/	meep
ಠ_ಠ	meh
ಡ_ಡ	mistyeyes
༼ ༎ຶ ෴ ༎ຶ༽	monster
♮	natural
┌(◉ ͜ʖ◉)つ┣▇▇▇═──	needle
	inject
( ͡° ͜ °)	nice
→_←	no
／人◕ __ ◕人＼	noclue
(っˆڡˆς)	nom
	yummy
	delicious
♫	note
	sing
☢	nuclear
	radioactive
	nukular
~=[,,_,,]:3	nyan
@^@	nyeh
( º﹃º )	ohshit
◕_◕	omg
⅛	8-Jan
¼	4-Jan
½	2-Jan
⅓	3-Jan
⌥	opt
	option
(눈_눈)	orly
(◞థ౪థ)ᴖ	ohyou
	ou
✌(-‿-)✌	peace
	victory
(__>-	pear
π	pi
( •_•)O*¯`·.¸.·´¯`°Q(•_• )	pingpong
._.	plain
(˶‾᷄ ⁻̫ ‾᷅˵)	pleased
(☞ﾟヮﾟ)☞	point
ʕ •́؈•̀)	pooh
(•ᴥ• )́`́'́`́'́⻍	porcupine
£	pound
(☝ ՞ਊ ՞)☝	praise
O=('-'Q)	punch
t(ಠ益ಠt)	rage
	mad
(ノಠ益ಠ)ノ彡┻━┻	rageflip
(=^･ｪ･^=))ﾉ彡☆	rainbowcat
ò_ô	really
®	r
→	right
	->
୧༼ಠ益ಠ༽୨	riot
⚂	rolldice
(◔_◔)	rolleyes
✿ڿڰۣ—	rose
(╯°□°)╯	run
ε(´סּ︵סּ`)з	sad
ヽ༼ຈʖ̯ຈ༽ﾉ	saddonger
( ͡° ʖ̯ ͡°)	sadlenny
⅞	8-Jul
♯	sharp
	diesis
╚(•⌂•)╝	shout
¯\_(ツ)_/¯	shrug
=^_^=	shy
Σ	sigma
	sum
☠	skull
ツ	smile
☺︎	smiley
¬‿¬	smirk
☃	snowman
(;´༎ຶД༎ຶ`)	sob
♠	spade
√	sqrt
<コ:彡	squid
★	star
ᕙ(⇀‸↼‶)ᕗ	strong
ε/̵͇̿̿/’̿’̿ ̿(◡︵◡)	suicide
∑	sum
☀	sun
(๑•́ ヮ •̀๑)	surprised
\_(-_-)_/	surrender
┬┴┬┴┤(･_├┬┴┬┴	stalker
(̿▀̿‿ ̿▀̿ ̿)	swag
o()xxxx[{::::::::::::::::::>	sword
┬─┬ ノ( ゜-゜ノ)	tabledown
(ノ ゜Д゜)ノ ︵ ┻━┻	tableflip
τ	tau
(ಥ﹏ಥ)	tears
୧༼ಠ益ಠ༽︻╦╤─	terrorist
\(^-^)/	thanks
	thankyou
	ty
⸫	therefore
	so
⅜	8-Mar
|=-(¤)-=|	tiefighter
(=____=)	tired
☜(꒡⌓꒡)	toldyouso
	toldyou
ᕦ(òᴥó)ᕥ	toogood
™	tm
▲	triangle
	t
⅔	3-Feb
┬──┬ ノ(ò_óノ)	unflip
↑	up
(๑•̀ㅂ•́)ง✧	victory
(ÒДÓױ)	wat
( * ^ *) ノシ	wave
Ö	whaa
(っ^з^)♪♬	whistle
(°o•)	whoa
ლ(`◉◞౪◟◉‵ლ)	why
WHΣИ $HΛLL WΣ †HЯΣΣ MΣΣ† ΛGΛ|И?	witchtext
＼(＾O＾)／	woo
(⊙＿⊙')	wtf
⊙ω⊙	wut
\( ﾟヮﾟ)/	yay
(•̀ᴗ•́)و ̑̑	yeah
	yes
¥	yen
☯	yinyang
	yy
Yᵒᵘ Oᶰˡʸ Lᶤᵛᵉ Oᶰᶜᵉ	yolo
ლ༼>╭ ͟ʖ╮<༽ლ	youkids
	ukids
(屮ﾟДﾟ)屮 Y U NO	y u no
	yuno
⊹╰(⌣ʟ⌣)╯⊹	zen
	meditation
	omm
(V) (°,,,,°) (V)	zoidberg
[¬º-°]¬	zombie
```
